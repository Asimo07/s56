import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights(){
	return (
		<Row className="mt-3 mb-3 xs-col-6">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				  
				  <Card.Body>
				    <Card.Title>
				    	<h2>Female Section</h2>
				    </Card.Title>
				    <Card.Text>
				      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
				    </Card.Text>
				    
				  </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				  <Card.Body>
				    <Card.Title>
				    	<h2>Male Section</h2>
				    </Card.Title>
				    <Card.Text>
				      Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				  <Card.Body>
				    <Card.Title>
				    	<h2>Kid Section</h2>
				    </Card.Title>
				    <Card.Text>
				     	Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>
		</Row>
	)
}
