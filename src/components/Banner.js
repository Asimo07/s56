import { Button, Row, Col } from 'react-bootstrap';

export default function Banner(){
	
		return (
			<Row>
				<Col className="p-5 xs-col-6">
					<h1>Your Shop</h1>
					<p>You can find everything here</p>
					<Button variant="primary">Order now!</Button>
				</Col>
			</Row>
		)
}
