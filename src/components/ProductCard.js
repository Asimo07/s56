import PropTypes from 'prop-types'
import { Card, Button } from 'react-bootstrap';

export default function ProductCard({productProp}) {

    const {name, description, price} = productProp;

    return (
        <Card className="mt-5 mb-5 xs-col-6">
            <Card.Body >
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP {price}</Card.Text>
                <Button variant="primary">Add to Cart</Button>
            </Card.Body>
        </Card>
    )
}

ProductCard.propTypes = {
    
    product: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}
