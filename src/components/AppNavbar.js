import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink } from 'react-router-dom';

// AppNavbar component
export default function AppNavbar(){
	return(
		<Navbar bg="light" variant="light" className="xs-col-6">
		    <Navbar.Brand as={NavLink} to="/" exact>Icon</Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav">
		    	<Nav className="ml-auto">
		      		<Nav.Link as={NavLink} to="/" exact>Home</Nav.Link>
		      		<Nav.Link as={NavLink} to="/products" exact>Products</Nav.Link>
		      		<Nav.Link as={NavLink} to="/login" exact>Login</Nav.Link>
		      		<Nav.Link as={NavLink} to="/register" exact>Register</Nav.Link>
		    	</Nav>
		   </Navbar.Collapse> 
	  	</Navbar>
	)
}

