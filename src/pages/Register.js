import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Redirect, useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Register(){

	const history = useHistory();

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState('');

	function registerUser(e) {
		e.preventDefault();

		fetch('http://localhost:4000/users/register', {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                mobileNo: mobileNo,
                password: password
            })
		})
		.then(res => res.json())
		.then(data => {

		console.log(data);

		 if(data === true){

		    setFirstName('');
		    setLastName('');
		    setEmail('');
		    setMobileNo('');
		    setPassword('');

		 Swal.fire({
                    title: 'Registration successful',
                    icon: 'success',
                    text: 'Welcome to Zuitt!'
                });

                        
                history.push("/login");

            } else {

                Swal.fire({
                    title: 'Something wrong',
                    icon: 'error',
                    text: 'Please try again.'   
                });
            };
        })
    };
    
    useEffect(() => {

            // Validation to enable submit button when all fields are populated and both passwords match
            if((firstName !== '' && lastName !== '' && email !== '' && mobileNo.length === 11 && password !== '')){
                setIsActive(true);
            } else {
                setIsActive(false);
            }

        }, [firstName, lastName, email, mobileNo, password]);

        return (
                <Form onSubmit={(e) => registerUser(e)}>
                	<h3 className="mt-5 mb-5">Register</h3>
                    <Form.Group controlId="firstName" className="mt-3 mb-3">
                        <Form.Control 
                            type="text" 
                            placeholder="Enter first name"
                            value={firstName} 
                            onChange={e => setFirstName(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="lastName" className="mt-3 mb-3">
                        <Form.Control 
                            type="text" 
                            placeholder="Enter last name"
                            value={lastName} 
                            onChange={e => setLastName(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="userEmail" className="mt-3 mb-3">
                        <Form.Control 
        	                type="email" 
        	                placeholder="Enter email"
                            value={email} 
                            onChange={e => setEmail(e.target.value)}
        	                required
                        />
                    </Form.Group>

                    <Form.Group controlId="mobileNo" className="mt-3 mb-3">
                        <Form.Control 
                            type="text" 
                            placeholder="Enter Mobile Number"
                            value={mobileNo} 
                            onChange={e => setMobileNo(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="password" className="mt-3 mb-3">
                        <Form.Control 
        	                type="password" 
        	                placeholder="Password"
                            value={password} 
                            onChange={e => setPassword(e.target.value)}
        	                required
                        />
                    </Form.Group>


                        <Button variant="primary" type="submit" id="submitBtn" className="mt-3 mb-3">
                            Submit
                        </Button>
                	</Form>
        )
        
    }
    


