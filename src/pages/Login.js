import { Form, Button } from 'react-bootstrap';

export default function Login() {
    return (
        <Form>
        	<h3>Login</h3>
            <Form.Group controlId="userEmail" className="mt-3 mb-3">
            <Form.Control type="" placeholder="Enter email" required />
            </Form.Group>

            <Form.Group controlId="password" className="mt-3 mb-3">
            <Form.Control type="password" placeholder="Password" required />
            </Form.Group>
            		  
            <Button variant="primary" type="submit" id="submitBtn">
             	Enter
            </Button>
            
        </Form>
    )
}
