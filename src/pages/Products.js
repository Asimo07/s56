import { Fragment } from 'react';
import ProductCard from '../components/ProductCard';
import productsData from '../data/productsData';

export default function Products(){
	const products = productsData.map(product => {
			return(
				<ProductCard key={product.id} productProp={product} />
			)
		})

	return (
		<Fragment>
			{products}
		</Fragment>
	)
}
