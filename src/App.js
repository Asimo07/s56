import { Fragment } from 'react';
import { Container } from 'react-bootstrap'
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import './App.css';

function App() {
  return (
    <Router>
      <AppNavbar />
      <Container>
      	<Switch>
      		<Route exact path="/" component={Home} />
      		<Route exact path="/products" component={Products} />
      		<Route exact path="/register" component={Register} />
      		<Route exact path="/login" component={Login} />
      	</Switch>
      </Container>
    </Router>
  );
}

export default App;
